import json
import log_handler
import sys


class ConfigLoad(log_handler.Logger):

    def __init__(self, log_path, config_path):
        super().__init__(log_path)
        try:
            self.data = json.load(open(config_path))
            self.log_event(75 * "-")
            self.log_event(f"Log {config_path} loaded successfully.")
        except FileNotFoundError as err:
            self.log_event(f"Log {config_path} doesn't exist - {err}")
            sys.exit(1)

    def load_data(self, key):
        try:
            result = self.data[key]
            return result
        except KeyError as err:
            self.log_event(f"Requested value doesn't exist - {err}")
            sys.exit(1)

    def save_json_data(self, data_to_save, file_path):
        try:
            with open(file_path, 'w') as fd:
                json.dump(data_to_save, fd, indent=2)
        except FileNotFoundError as err:
            self.log_event(f"Log {file_path} doesn't exist - {err}")
            sys.exit(1)

    def open_json_file(self, file_path):
        try:
            with open(file_path, 'r') as rf:
                fobj = json.load(rf)
            return fobj
        except FileNotFoundError as err:
            self.log_event(f"Log {file_path} doesn't exist - {err}")
            sys.exit(1)



if __name__ == "__main__":
    cfg_load = ConfigLoad('logs/process.log', 'config/config.json')
    res = cfg_load.load_data('request_config')
    cfg_load.save_json_data({"online_machines": ["10.10.10.10", "11.11.11.11"]}, "test/save_config.json")
    ob = cfg_load.open_json_file('test/save_config.json')
