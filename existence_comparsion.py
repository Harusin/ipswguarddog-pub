from subprocess import PIPE, Popen
import load_configs
import hashlib


class IpswExistance(load_configs.ConfigLoad):
    """Check if local images needs to be updated or even exist alco check if image is complete."""
    def __init__(self, log_path, config_path, firmware_data_path):
        super().__init__(log_path, config_path)
        self.fw_data = firmware_data_path

    def md_five_check(self, file_name):
        """Insert image name return MD5."""
        loc_path = self.load_data('dir_path')['local_ipsw_dir'] + file_name
        block_size = 65536
        hasher = hashlib.md5()
        with open(loc_path, 'rb') as afile:
            buf = afile.read(block_size)
            while len(buf) > 0:
                hasher.update(buf)
                buf = afile.read(block_size)
        md5 = hasher.hexdigest()
        return md5

    def get_local_images(self):
        """Return list of local existing images, specify address with wildcard
        at the end 'local_path/*.ipsw' this format is not required.
        """
        ipsw_dir = self.load_data('dir_path')['local_ipsw_dir']
        execute_command = Popen(['ls', ipsw_dir], stdout=PIPE, stderr=PIPE)
        return_output = execute_command.communicate()[0]
        formated_out = return_output.decode('utf-8')
        ls_res = formated_out.split("\n")
        ls_res.remove('')
        ex_result = {element.split("/")[-1] for element in ls_res}
        return ex_result

    def get_remote_images(self):
        """Load remote firmware data."""
        firmware_data = self.open_json_file(self.fw_data)
        remote_set = {v[1] for v in firmware_data['fwdata'].values()}
        fw_sum_dict = {v[1]: v[2] for v in firmware_data['fwdata'].values()}
        return fw_sum_dict, remote_set

    def compare_checksum(self):
        local_images = self.get_local_images()
        remote_images_sum, remote_images = self.get_remote_images()
        compared = {}
        for image in local_images:
            if image in remote_images_sum.keys():
                start = self.time_start()
                self.log_event(f"Enumerationg checksum for image {image}")
                local_md5 = self.md_five_check(image)
                if local_md5 == remote_images_sum[image]:
                    self.log_event(f"Local MD5: {local_md5} -> Remote MD5: {remote_images_sum[image]}")
                    compared[image] = True
                else:
                    self.log_event(f"Local MD5: {local_md5} -> Remote MD5: {remote_images_sum[image]}")
                    compared[image] = False
                end = self.time_end(start)
                self.log_event(f"Check took {end}")
        return compared

    def is_uptodate(self):
        """Check if downloaded images are up-to-date."""
        local_images = self.get_local_images()
        remote_images_sum, remote_images = self.get_remote_images()
        img_diff = set(remote_images).difference(local_images)
        img_inter = set(remote_images).intersection(local_images)
        return img_diff, img_inter

    def main(self, case_switch):
        if case_switch == 'uptodate':
            idf, imn = self.is_uptodate()
            return idf, imn
        elif case_switch == 'checksum':
            compared = self.compare_checksum()
            return compared


if __name__ == '__main__':
    ipswex = IpswExistance('logs/local_remote_comp.log', 'config/config.json', 'config/firmware.json')
    idf, imn = ipswex.main('uptodate')
    comp = ipswex.main('checksum')
    print(comp)
    pass
    # IpswExistance.md_five_check("assets/image/iPhone10,3,iPhone10,6_12.0_16A366_Restore.ipsw")
