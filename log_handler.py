import logging
import datetime
import time
import math
import sys


class Logger:
    def __init__(self, log_path):
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        handler = logging.FileHandler(log_path)
        handler.setLevel(logging.INFO)
        formatter = logging.Formatter(
            '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)
        self.clean_log(log_path)

    def log_event(self, context):
        self.logger.info(context)

    @staticmethod
    def time_start():
        return time.time()

    @staticmethod
    def time_end(t_start):
        t_end = time.time() - t_start
        res = str(datetime.timedelta(seconds=math.floor(t_end)))
        hh, mm, ss = res.split(':')
        res = ("{0}h:{1}m:{2}s".format(hh, mm, ss))
        return res

    @staticmethod
    def clean_log(log_path):
        try:
            file = open(log_path, "w")
            file.close()
        except (FileNotFoundError, FileExistsError):
            sys.exit(1)


if __name__ == "__main__":
    logg = Logger("test/log.log")
    logg.log_event("new event")
