# IpswDownload

Rewrite of ipswGuardDog bash script to more functional form

## Api parse module [api_parse.py]

- module contains all http requests and outputing prepared links for download

- complete 100%

## Download module [ipsw_download.py]

- asynchronous download of new images to assets/image/*.ipsw

- complete 100%

## Availability check [availability_check.py]

- based on host_list.json checks for all online stations results with "online_machines.log" and "online_machines.json"

- complete 100%

## [sync_ssh.py] [async_ssh.py]

- self-explanatory

- complete 100% (but takes too long torrent, sockets or websockets would be much better option)

## Existence comparsion [existence_comparsion.py]

- check if images alreary exists with checksum md5 check using generated file firmware.json

- complete 80% (missing implementation with ipsw_download.py and api_parse.py) basically remove all false from download_payload

## Load configs [load_configs.py]

- multipurpose json handler mainly used to read data from main config file "config/config.json"

## Log handler [log_handler.py]

- log function and time function (allows to measure time of operations)

## Network speeds [network_speeds.py]

- Can measure from one server to all hosts i doubt usefulness of this script

- complete 100% 

## Plist parse [plist_parse.py]

- Autonomous "Settings.plist" updater. File must be present at "config/Settings.plist" this will only rename images with new image name but won't create additional lines for new models

## Main function [main.py]

- All parts of whole aplication should be run respectively from this module

### Main configuration file [config/config.json]

- ssh_auth -> ssh authentication
- local_ip -> ip of computer which is running network_speed.py
- dir_path -> necessary paths for iperf and images which can be expanded with log and json addresses
- request_config -> All models should be listed here for download
- settings_plist_map -> mapping of all models which have same ipsw image necessary for plist_parse.py
- api_ipsw_link -> necessary for api_parse 
