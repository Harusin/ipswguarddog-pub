import urllib.request
import urllib.error
import threading
from queue import Queue
import ssl
import log_handler


class ImageDownload(log_handler.Logger):
    """Download images from specified url path."""

    def __init__(self, log_path, urls):
        super().__init__(log_path)
        ssl._create_default_https_context = ssl._create_unverified_context
        self.dl_lock = threading.Lock()
        self.url_queue = Queue()
        self.urls = urls

    def get_file(self, current_url):
        with self.dl_lock:
            imgname = self.urls[current_url].split("/")[-1]
            self.log_event("Download started for image: %s" % imgname)
        try:
            urllib.request.urlretrieve(current_url, self.urls[current_url])
        except urllib.error.HTTPError as err:
            self.log_event(75 * "-")
            self.log_event(f"Image {imgname} failed with error - \n {err}")
            self.log_event(75 * "-")
        except urllib.error.URLError as err:
            self.log_event(75 * "-")
            self.log_event(f"Image {imgname} failed with error - \n {err}")
            self.log_event(75 * "-")
        with self.dl_lock:
            self.log_event("Download has ended for image: %s" % imgname)
        self.log_event(75 * "-")

    def process_queue(self):
        while True:
            current_url = self.url_queue.get()
            self.get_file(current_url)
            self.url_queue.task_done()

    def main(self):
        start_time = self.time_start()
        self.log_event(75 * "-")
        self.log_event("Initializing downlaod process.")
        self.log_event(75 * "-")
        for i in range(len(self.urls)):
            t = threading.Thread(target=self.process_queue)
            t.daemon = True
            t.start()

        for current_url, file_name in self.urls.items():
            self.url_queue.put(current_url)

        self.url_queue.join()
        end_time = self.time_end(start_time)
        self.log_event(75 * "-")
        self.log_event('All downloads took: %s' % end_time)


if __name__ == '__main__':
    pass
