import socket
import ipaddress
import load_configs
import re


class AviabilityCheck(load_configs.ConfigLoad):

    def __init__(self, log_path, config_path, host_key, port, timeout):
        super().__init__(log_path, config_path)
        self.host_key = host_key
        self.port = port
        self.timeout = timeout

    def address_validity(self, address):
        """Confirm that recieved address is in valid format."""
        try:
            ipaddress.ip_address(address)
            return True
        except ValueError as err:
            self.log_event(f"Format of address {address} wasn't correct - exited with error {err}")
            return False

    def on_off(self):
        """Check for network availability. Report offline stations"""
        host_dict = self.load_data(self.host_key)
        online_hosts = []
        for name, ip in host_dict.items():
            address_valid = self.address_validity(ip)
            if address_valid:
                client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                client.settimeout(self.timeout)
                try:
                    client.connect((ip, self.port))
                    response = client.recv(19)
                    response = str(response, 'utf-8')
                    if len(response) > 0:
                        online_hosts.append(ip)
                        client.close()
                except socket.timeout as err:
                    self.log_event(f"Computer with Name: {name} || IP: {ip} || Response error: {err}")
        if len(online_hosts) == 0:
            online_hosts.append("EMPTY")
        return online_hosts

    @staticmethod
    def scope_filter(input_data):
        """Filter ips by scope."""
        raw_scopes = {}
        for ip_add in input_data:
            re.findall(r"\d+\D\d+\D(\d+)", ip_add)
            raw_scopes[ip_add] = re.findall(r"\d+\D\d+\D(\d+)", ip_add)[0]
        return raw_scopes

    def online_stations_gen(self, online_hosts):
        """Create file with online machines sorted by scope."""
        raw_scopes = self.scope_filter(online_hosts)
        scope_set = set(raw_scopes.values())
        self.log_event(75 * "-")
        self.log_event(f"Units found in active scopes {scope_set}")
        self.log_event("Saving scopes to JSON file.")
        scopes_result = {}
        for val in scope_set:
            sc = [k for k, v in raw_scopes.items() if v == val]
            scopes_result[val] = sc
        self.save_json_data({"online_machines": scopes_result}, "config/online_machines.json")
        self.log_event("All data saved.")
        self.log_event(75 * "-")

    def main(self):
        # scope = {'10.172.11.4': '11', '10.172.12.95': '12', '10.172.13.92': '13', '10.172.14.96': '14'}
        online_hosts = self.on_off()
        self.online_stations_gen(online_hosts)



if __name__ == '__main__':
    achck = AviabilityCheck('logs/online_machines.log', 'config/host_list.json', 'all', 22, 3)
    achck.main()
