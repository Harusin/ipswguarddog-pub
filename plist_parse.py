import plistlib
import load_configs


class PlistParse(load_configs.ConfigLoad):
    def __init__(self, log_path, config_path, download_payload):
        super().__init__(log_path, config_path)
        self.dpld = download_payload

    def boardconf_req(self):
        result = []
        bid = self.load_data('settings_plist_map')
        firmwares = self.open_json_file('config/firmwares.json')
        for key, value in self.dpld.items():
            identifier_s = key.split("/")[-2]
            imagename_s = value.split("/")[-1]
            for i in firmwares['firmwares']:
                if i['identifier'] == identifier_s:
                    for value in bid.values():
                        if i['boardconfig'] in value:
                            result.append((value, imagename_s))
        return result

    def create_new_plist(self, setting_path):
        bcfg = self.boardconf_req()
        with open(setting_path, 'rb') as fp:
            pl = plistlib.load(fp)
        for item in bcfg:
            for boardcfg in item[0]:
                pl['General']['BundleMap'][boardcfg] = item[1]
        with open(setting_path, 'wb') as fp:
            plistlib.dump(pl, fp)


if __name__ == "__main__":
    payload = {
        'https://api.ipsw.me/v4/ipsw/download/iPhone6,2/16A404': 'assets/image/iPhone_4.0_64bit_12.0.1_16A404_Restore.ipsw',
        'https://api.ipsw.me/v4/ipsw/download/iPhone8,1/16A404': 'assets/image/iPhone_4.7_12.0.1_16A404_Restore.ipsw',
        'https://api.ipsw.me/v4/ipsw/download/iPhone8,2/16A404': 'assets/image/iPhone_5.5_12.0.1_16A404_Restore.ipsw',
        'https://api.ipsw.me/v4/ipsw/download/iPhone9,1/16A404': 'assets/image/iPhone_4.7_P3_12.0.1_16A404_Restore.ipsw',
        'https://api.ipsw.me/v4/ipsw/download/iPhone9,2/16A404': 'assets/image/iPhone_5.5_P3_12.0.1_16A404_Restore.ipsw',
        'https://api.ipsw.me/v4/ipsw/download/iPhone10,6/16A404': 'assets/image/iPhone10,3,iPhone10,6_12.0.1_16A404_Restore.ipsw',
        'https://api.ipsw.me/v4/ipsw/download/iPhone11,6/16A405': 'assets/image/iPhone11,4,iPhone11,6_12.0.1_16A405_Restore.ipsw',
        'https://api.ipsw.me/v4/ipsw/download/iPhone11,2/16A405': 'assets/image/iPhone11,2_12.0.1_16A405_Restore.ipsw'}

    cfg_load = PlistParse('logs/settings.log', 'config/config.json', payload)
    cfg_load.create_new_plist('config/Settings.plist')
    pass