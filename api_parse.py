import urllib.request
import urllib.error
import ssl
import tempfile
import shutil
import json
import load_configs
import re
import sys


class ApiInterface(load_configs.ConfigLoad):

    def __init__(self, log_path, config_path):
        super().__init__(log_path, config_path)

    @staticmethod
    def get_date_number(date):
        dnum = re.findall(r'\d+\S\d+\S\d+', date)
        dnum = dnum[0].replace("-", "")
        return int(dnum)

    @staticmethod
    def format_date(dnum):
        dnum = str(dnum)
        dnum = str(dnum[:4]) + '-' + str(dnum[4:6]) + '-' + str(dnum[6:8])
        return dnum

    def retrieve_data(self, url_link):
        """Retrieve data from url link."""
        try:
            ssl._create_default_https_context = ssl._create_unverified_context
            with urllib.request.urlopen(url_link) as response:
                with tempfile.NamedTemporaryFile(delete=False) as tmp_file:
                    shutil.copyfileobj(response, tmp_file)
            with open(tmp_file.name) as html:
                requested_data = html.read()
            return requested_data
        except urllib.error.HTTPError as err:
            self.log_event(f"Unable to GET requested data error 404 - \n {err}")
            sys.exit(1)

    def response_data_handle(self, url_link):
        """Handle response turn it to json."""
        req_data = self.retrieve_data(url_link)
        requested_data = '{ "firmwares": ' + req_data + '}'
        str_to_json = json.loads(requested_data)
        self.save_json_data(str_to_json, "config/firmwares.json")
        return str_to_json

    def get_devices(self, url_link):
        """Return identifier list for all models [iPhone1.1,]"""
        rdata = self.response_data_handle(url_link)
        identifiers = [i['identifier'] for i in rdata['firmwares']
                       if i['boardconfig'] in self.load_data('request_config')]
        return identifiers

    def filter_actual(self, request_data):
        """Filter all actual images from requested data."""
        all_firmwares = json.loads(request_data)["firmwares"]
        all_signed = [i for i in all_firmwares if i['signed'] is True]
        latest_date = 0
        for record in all_signed:
            date_num = self.get_date_number(record['releasedate'])
            if date_num > latest_date:
                latest_date = date_num
        ldate_formated = self.format_date(latest_date)
        newer_rec = [
            i for i in all_signed if i['releasedate'].startswith(ldate_formated)]
        try:
            return newer_rec[0]
        except IndexError:
            return 'Index Error'

    @staticmethod
    def store_latest_image_names(image_names):
        """Store all new image names to json file."""
        with open('config/api_image_names.json', "w") as f:
            json.dump({"remote_ipsw_files": list(image_names)}, f, sort_keys=True, indent=2)

    def get_firmware(self, url, identifiers, fw_dump_path):
        """Get identifier, buildID and name of image. Pack it to dictionary and remove duplicate images."""
        clean_res = {}
        results = {}
        img_names = set()
        identifier_link = [url + i + "?type=ipsw" for i in identifiers]
        for link in identifier_link:
            firmware_request = self.retrieve_data(link)
            latest_firmware = self.filter_actual(firmware_request)
            clean_res[latest_firmware['identifier']] = (
                latest_firmware['buildid'], latest_firmware['url'].split("/")[-1], latest_firmware['md5sum'])
            img_names.add(latest_firmware['url'].split("/")[-1])
        self.store_latest_image_names(img_names)
        for key, value in clean_res.items():
            if value not in results.values():
                results[key] = value
        self.save_json_data({'fwdata': results}, fw_dump_path)
        return results

    @staticmethod
    def download_links(url, fw_resources, dl_path):
        """Create dictionary of image download links."""
        dl_payload = {}
        for identifier, value in fw_resources.items():
            build = value[0]
            url_link = url + identifier + "/" + build
            dl_payload[url_link] = dl_path + value[1]
        return dl_payload

    def main(self):
        """Process all api requests."""
        start = self.time_start()
        links = self.load_data('api_ipsw_link')
        self.log_event("Starting parsing process.")
        self.log_event(75 * "-")
        identifiers = self.get_devices(links['api_link_devices'])
        firmware = self.get_firmware(links['api_link_device'], identifiers, 'config/firmware.json')
        for k, v in firmware.items():
            self.log_event(f"{k} || {v}")
            self.log_event(75 * "-")
        download_path = self.load_data('dir_path')['local_ipsw_dir']
        download_payload = self.download_links(links['api_link_download'], firmware, download_path)
        end = self.time_end(start)
        self.log_event("All API responses were sucessful.")
        self.log_event(f"Total time: {end}")
        return download_payload


if __name__ == '__main__':
    apint = ApiInterface('logs/process.log', 'config/config.json')
    payload = apint.main()
    print(payload)
    print(len(payload))
    pass
