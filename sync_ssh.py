import paramiko
import os
import os.path
import socket

class SyncRemote:
    """Connection and transmission of files over SSH."""

    def __init__(self):
        self.port = 22
        self.timeout = 3

    def ssh_command(self, remote_ip, user, pwd, bash_command):
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(remote_ip,
                    port=self.port,
                    username=user,
                    password=pwd,
                    timeout=self.timeout)
        stdin, stdout, stderr = ssh.exec_command(bash_command)
        out = stdout.readlines()
        err = stderr.readlines()
        ssh.close()
        return out, err

    def sftp_transport(self, host, user, pwd, local_file_path, remote_file_path):
        """Estabilish connection and send file to destination."""
        try:
            tran = paramiko.Transport(host, self.timeout)
            tran.connect(username=user, password=pwd)
            sftp = paramiko.SFTPClient.from_transport(tran)
            sftp.put(local_file_path, remote_file_path)
            sftp.close()
        except (paramiko.ssh_exception.SSHException, FileNotFoundError, socket.timeout):
            print(f"{host} error")
            pass

    def online_offline(self, remote_ip):
        """Checking if station is online or offline."""
        response = os.system('ping -c 1 -t 1 ' + remote_ip + ' > /dev/null')
        if response == 0:
            return True
        elif response == 512:
            return False
        else:
            return response


if __name__ == "__main__":
    ssh_s = SyncRemote()
    hostArr = [
        "10.172.11.4",
        "10.172.11.6",
        "10.172.11.8",
        "10.172.11.66",
        "10.172.11.3",
        "10.172.11.5",
        "10.172.11.7",
        "10.172.11.67",
        "10.172.11.86",
        "10.172.11.25",
        "10.172.11.27",
        "10.172.11.29",
        "10.172.11.68",
        "10.172.11.24",
        "10.172.11.26",
        "10.172.11.28",
        "10.172.11.69",
        "10.172.11.87"
    ]
    for i in hostArr:
        # out, err = ssh_s.ssh_command(i, "user", "password",
        #                              "ln /Users/pcz/Temporary/*.ipsw /Users/pcz/Library/iTunes/iPhone\ Software\ Updates/")
        # print(f"{i}: {out}, {err} ")

        # out, err = ssh_s.ssh_command(i, "user", "password",
        #                              "killall /Applications/PurpleRabbit.app")
        # print(f"{i}: {out}, {err} ")

        # out, err = ssh_s.ssh_command(i, "user", "password",
        #                              "ls ~/")
        # print(f"{i}: {out}, {err} ")

        # ssh_s.sftp_transport(i, "user", "password", "assets/PR/Settings.plist",
        #                      "/Users/user/Library/Application Support/PurpleRabbit/Settings.plist")
        # ssh_s.sftp_transport(i, "user", "password", "assets/image/iPhone11,2_12.0_16A366_Restore.ipsw",
        #                      "/Users/user/Temporary/iPhone11,2_12.0_16A366_Restore.ipsw")
        # ssh_s.sftp_transport(i, "user", "password", "assets/image/iPhone11,4,iPhone11,6_12.0_16A366_Restore.ipsw",
        #                      "/Users/user/Temporary/iPhone11,4,iPhone11,6_12.0_16A366_Restore.ipsw")
        pass
