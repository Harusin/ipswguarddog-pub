import asyncio
import asyncssh
import json
import os


class AsyncRemote:
    """Implement both SSH/SFTP types Asynchronous and Synchronous connection."""

    async def run_ssh_client(self, host, command, user, pwd):
        """Start asynchronous SSH connection, part of async_ssh_clients."""
        async with asyncssh.connect(host, username=user, password=pwd, known_hosts=None) as conn:
            return await conn.run(command)

    async def async_ssh_clients(self, hosts, command, user, pwd):
        """Run asynchronous SSH clients connection."""
        ssh_success = {}
        tasks = (self.run_ssh_client(host, command, user, pwd)
                 for host in hosts)
        results = await asyncio.gather(*tasks, return_exceptions=True)
        for i, result in enumerate(results, 1):
            if isinstance(result, Exception):
                pass
            elif result.exit_status != 0:
                pass
            else:
                output = result.stdout
                ip = output.split("\n")[0]
                outputdata = output.split("\n")
                outputdata.remove(ip)
                ssh_success[ip] = outputdata
        return ssh_success

    async def run_sftp_client(self, host, local_file_path, remote_file_path, user, pwd):
        """Start aasynchronous SFTP connection, part of async_sftp_clients."""
        async with asyncssh.connect(host, username=user, password=pwd, known_hosts=None) as conn:
            async with conn.start_sftp_client() as sftp:
                return await sftp.put(local_file_path, remotepath=remote_file_path)

    async def async_sftp_clients(self, hosts, local_file_path, remote_file_path, user, pwd):
        """Asynchronous SFTP clients connection."""
        tasks = (self.run_sftp_client(host, local_file_path, remote_file_path, user, pwd)
                 for host in hosts)
        result = await asyncio.gather(*tasks, return_exceptions=True)
        return result

    def get_remote_file_names(self, command_result):
        """Return Temporary folder contents (ipsw files if exists) for each computer."""
        formated_dict = {}
        _outputdata = []
        for key, value in command_result:
            try:
                _outputdata.append(value.split("/")[-1])
                _outputdata.remove('')
            except (AttributeError, ValueError):
                pass
            formated_dict[key] = _outputdata
        return formated_dict

    def process_async_ssh(self, hosts, command, user, pwd):
        """Run SSH main function for ssh."""
        results = asyncio.get_event_loop().run_until_complete(
            self.async_ssh_clients(hosts, command, user, pwd))
        print(results)
        return results

    def process_async_sftp(self, hosts, local_file_path, remote_file_path, user, pwd):
        """Run SFTP main function for sftp."""
        try:
            asyncio.get_event_loop().run_until_complete(
                self.async_sftp_clients(hosts, local_file_path, remote_file_path, user, pwd))
        except (OSError, asyncssh.Error) as exc:
            print('SSH connection failed: ' + str(exc))

if __name__ == '__main__':
    pass
