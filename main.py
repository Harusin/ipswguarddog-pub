import api_parse
import ipsw_download
import availability_check
import existence_comparsion
import plist_parse



#TODO
"""

1. api_parse -> gather informations about images and check for new image viability

2. check mac mini for availability

3. e


"""


class Main:
    def __init__(self):
        pass


if __name__ == '__main__':
    # Generate downlaod links and and paths
    apint = api_parse.ApiInterface('logs/api_parse.log', 'config/config.json')
    download_payload = apint.main()
    # Create Settings.plist with latest images
    cfg_load = plist_parse.PlistParse('logs/settings.log', 'config/config.json', download_payload)
    cfg_load.create_new_plist('config/Settings.plist')
    # Check and write down available images
    # avchk = availability_check.AviabilityCheck('logs/station_availability.log',
    #                                            'config/host_list.json', 'all', 22, 3)
    # avchk.main()
    # Start download process
    # existcomp = existence_comparsion.IpswExistance('log/existanance.log',
    #                                                'config/config.json', 'config/firmware.json')
    imgdl = ipsw_download.ImageDownload('logs/img_download.log', download_payload)
    imgdl.main()
    pass