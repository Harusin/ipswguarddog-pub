import os
import json
import subprocess
import asyncio
import re
import asyncssh
import sys
import log_handler
import async_ssh
import sync_ssh
import load_configs


class NetworkSpeed:

    def __init__(self):
        self.log = log_handler.Logger('network_speed')
        self.wd = os.path.dirname(os.path.realpath(__file__)) + "/"

    def start_server(self, local_ip, iperf_binary_location):
        """Start iPerf local server."""
        i_server = [self.wd + iperf_binary_location,
                    "-f", "M", "-s", local_ip]
        self.proc = subprocess.Popen(i_server)
        print('Server opened')

    def close_server(self):
        if self.proc.poll() is None:
            self.proc.terminate()
        print('Server closed.')

    def send_iperf_bin(self, host_list, lfp, rfp):
        """Send iperf application to clients."""
        try:
            asyncio.get_event_loop().run_until_complete(
                nsp.async_sftp_clients(host_list, lfp, rfp))
        except (OSError, asyncssh.Error) as exc:
            sys.exit('SSH connection failed: ' + str(exc))

    def create_payload(self):
        """Create iperf folder in $HOME."""
        pass


if __name__ == "__main__":
    log = log_handler.Logger('network_speed')
    wd = os.path.dirname(os.path.realpath(__file__)) + "/"
    cfg_load = load_configs.ConfigLoad('config/config.json')
    cfg_data = cfg_load.load_json_file('config/config.json')
    username = cfg_data['ssh_auth']['user']
    password = cfg_data['ssh_auth']['pwd']
    r_iperf_dir = cfg_data['dir_path']['remote_iperf_dir']
    l_iperf_dir = cfg_data['dir_path']['local_iperf_dir']
    # Async sftp
    # Create iperf folder in $HOME
    log.log_event(75 * '-')
    start = log.time_start()
    hosts_data = cfg_load.load_json_file('config/host_list.json')
    hosts = hosts_data['all']
    print(list(hosts.values()))
    async_shell = async_ssh.AsyncRemote()
    create_payload_dir = 'mkdir -p ' + r_iperf_dir
    ssh_results = async_shell.process_async_ssh(
        list(hosts.values()),
        create_payload_dir,
        username,
        password
    )
    end = log.time_end(start)
    log.log_event('Remote directories created successfully in %s' % end)
    # Transfer iperf binary to remote computer
    log.log_event(75 * '-')
    start = log.time_start()
    transfer_res = async_shell.process_async_sftp(
        list(hosts.values()),
        l_iperf_dir,
        r_iperf_dir,
        username,
        password
    )
    print(transfer_res)
    # # Make remote iperf executable
    make_executable = 'echo ' + password + \
        ' | sudo -S chmod +x ' + r_iperf_dir + '/iperf3'
    ssh_results = async_shell.process_async_ssh(
        list(hosts.values()),
        make_executable,
        username,
        password
    )
    end = log.time_end(start)
    log.log_event('iPerf binary file copied in %s' % end)
    # # Measure Bandwith
    log.log_event(75 * '-')
    start = log.time_start()
    nsp = NetworkSpeed()
    # Start server locally
    nsp.start_server(cfg_data['local_ip']['local_ip_address'], l_iperf_dir)
    command = r_iperf_dir + '/iperf3' + ' -t 1 -f M -c 10.172.7.230'
    sync_shell = sync_ssh.SyncRemote()
    _speed_clean = open('config/speeds.json', 'w')
    _speed_clean.close()
    ip_dict = {}
    for name, ip in hosts.items():
        out, err = sync_shell.ssh_command(ip, username, password, command)
        print(out, err)
        speed_res = 0
        print(20*"*")
        print("Remote ip: " + ip)
        print(20*"*")
        speed = re.findall(r'sec\s+(\d+)', out[3].strip())
        print(speed)
        if float(speed[0]) > 30:
            speed_res = 1000
        else:
            speed_res = 100
        print(speed_res)
        ip_dict[name] = {ip: speed_res}
    with open('config/speeds.json', 'a') as fd:
        json.dump({"speed_results": ip_dict}, fd, indent=2)
    nsp.close_server()
    end = log.time_end(start)
    log.log_event('Speed measured in %s' % end)
